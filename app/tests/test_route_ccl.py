from fastapi.testclient import TestClient
from main import app

client = TestClient(app)


def test_get_ban_list():
    response = client.get("/platforms/1/ccl/current_season/ban_list")
    assert response.status_code == 401
    assert response.json()["detail"]["msg"] == "No API key provided."
