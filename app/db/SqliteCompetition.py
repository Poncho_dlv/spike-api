#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sqlite3


class SqliteCompetition:

    def __init__(self, server_id: str):
        self.__dbFileName = "databases/" + server_id + ".db"

    def get_competitions(self, league_id: int, platform_id: int):
        try:
            conn = sqlite3.connect(self.__dbFileName)
            cursor = conn.cursor()
            cursor.execute("""SELECT * FROM registeredCompetition WHERE cyanide_league_id=? AND platform_id=?""", (league_id, platform_id,))
            response = cursor.fetchall()
            conn.close()
            return response
        except sqlite3.OperationalError as e:
            print(self.__dbFileName)
            print(e)
            return None

    def is_competition_registered(self, competition_id: int, platform_id: int):
        try:
            conn = sqlite3.connect(self.__dbFileName)
            cursor = conn.cursor()
            cursor.execute("""SELECT id FROM registeredCompetition WHERE cyanide_id=? AND platform_id=?""", (competition_id, platform_id,))
            response = cursor.fetchone()
            conn.close()
            return response is not None
        except sqlite3.OperationalError as e:
            print(self.__dbFileName)
            print(e)
            return None

    def add_competition(self, competition_id: int, competition_name: str, channel_id: str, logo_link: str, emoji_id: str, platform_id: int,
                        cyanide_league_id: int):
        try:
            conn = sqlite3.connect(self.__dbFileName)
            cursor = conn.cursor()
            cursor.execute(
                """INSERT INTO registeredCompetition(cyanide_id, name, channel_id, logo_link, emoji, platform_id, cyanide_league_id) VALUES(?,?,?,?,?,?,?)""",
                (competition_id, competition_name, int(channel_id), logo_link, emoji_id, platform_id, cyanide_league_id,))
            conn.commit()
            conn.close()
        except sqlite3.OperationalError as e:
            print(self.__dbFileName)
            print(e)

    def remove_competition(self, competition_id: int, platform_id: int):
        try:
            conn = sqlite3.connect(self.__dbFileName)
            cursor = conn.cursor()
            ret = cursor.execute("""DELETE FROM registeredCompetition WHERE cyanide_id=? AND platform_id=?""", (competition_id, platform_id,))
            conn.commit()
            conn.close()
            return ret
        except sqlite3.OperationalError as e:
            print(self.__dbFileName)
            print(e)
            return None

    def remove_competition_in_league(self, league_id: int, platform_id: int):
        try:
            conn = sqlite3.connect(self.__dbFileName)
            cursor = conn.cursor()
            ret = cursor.execute("""DELETE FROM registeredCompetition WHERE cyanide_league_id=? AND platform_id=?""", (league_id, platform_id,))
            conn.commit()
            conn.close()
            return ret
        except sqlite3.OperationalError as e:
            print(self.__dbFileName)
            print(e)
            return None
