#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sqlite3


class SqliteLeague:

    def __init__(self, server_id: str):
        self.__dbFileName = "databases/" + server_id + ".db"

    def get_leagues(self):
        try:
            conn = sqlite3.connect(self.__dbFileName)
            cursor = conn.cursor()
            cursor.execute("""SELECT * FROM registeredLeagues""")
            response = cursor.fetchall()
            conn.close()
            return response
        except sqlite3.OperationalError as e:
            print(self.__dbFileName)
            print(e)
            return None

    def is_league_registered(self, league_id: int, platform_id: int):
        try:
            conn = sqlite3.connect(self.__dbFileName)
            cursor = conn.cursor()
            cursor.execute("""SELECT id FROM registeredLeagues WHERE cyanide_id=? AND platform_id=?""", (league_id, platform_id,))
            response = cursor.fetchone()
            conn.close()
            return response is not None
        except sqlite3.OperationalError as e:
            print(self.__dbFileName)
            print(e)
            return None

    def add_league(self, league_name: str, platform_id: int, cyanide_id: int, logo: str):
        try:
            conn = sqlite3.connect(self.__dbFileName)
            cursor = conn.cursor()
            cursor.execute("""INSERT INTO registeredLeagues(name, platform_id, cyanide_id, logo) VALUES(?,?,?,?)""",
                           (league_name, platform_id, cyanide_id, logo,))
            conn.commit()
            conn.close()
        except sqlite3.OperationalError as e:
            print(self.__dbFileName)
            print(e)

    def remove_league(self, league_id: int, platform_id: int):
        try:
            conn = sqlite3.connect(self.__dbFileName)
            cursor = conn.cursor()
            ret = cursor.execute("""DELETE FROM registeredLeagues WHERE cyanide_id=? AND platform_id=?""", (league_id, platform_id,))
            conn.commit()
            conn.close()
            return ret
        except sqlite3.OperationalError as e:
            print(self.__dbFileName)
            print(e)
            return None
