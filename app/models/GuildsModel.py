from pydantic import BaseModel


class CompRegistration(BaseModel):
    channel_id: str
    logo_label: str
