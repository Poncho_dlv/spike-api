from typing import Optional

from pydantic import BaseModel


class CompetitionsModelLeague(BaseModel):
    id: int
    name: str
    date_created: Optional[str]
    official: Optional[int]
    logo: Optional[str]
    registered_teams_count: Optional[int]


class CompetitionsModel(BaseModel):
    id: int
    name: str
    date_created: Optional[str]
    format: Optional[str]
    status: Optional[int]
    teams_max: Optional[int]
    teams_count: Optional[int]
    rounds_count: Optional[int]
    round: Optional[int]
    turn_duration: Optional[int]
    league: CompetitionsModelLeague
