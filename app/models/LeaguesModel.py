from pydantic import BaseModel


class LeaguesModel(BaseModel):
    id: int
    name: str
    date_last_match: str
    logo: str
    team_count: int
