from fastapi import APIRouter, Header, HTTPException, status

from routers import verify_token
from spike_requester.Utilities import Utilities

matches = APIRouter()
router_name = 'Matches'


@matches.get('/matches/{uuid}/full_data', tags=[router_name])
async def get_match(uuid: str, force_refresh: bool = False, api_key: str = Header(None, alias='API_KEY')):
    verify_token(api_key, 'admin.write')
    json_data = {'data': {}}

    data = Utilities.get_match(uuid, raw_data=True, force_refresh=force_refresh)

    if data is None:
        json_data['msg'] = 'Game not found.'
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=json_data)

    json_data['msg'] = 'Game found.'
    json_data['data'] = data
    return json_data
