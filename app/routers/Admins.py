from fastapi import APIRouter, Header

from routers import verify_token
from spike_database.DiscordGuild import DiscordGuild
from spike_database.Leagues import Leagues
from spike_utilities.Utilities import Utilities

admins = APIRouter()
router_name = "Admins"


@admins.delete("/clean_old_league", tags=[router_name])
async def clean_old_league(api_key: str = Header(None, alias="API_KEY")):
    verify_token(api_key, "admin.write")
    json_data = {"data": {}}

    excluded_server = Utilities.get_excluded_server()

    guilds_id = Utilities.get_guilds_id()
    league_db = Leagues()
    nb_cleared = 0

    for guild in guilds_id:
        try:
            if guild not in excluded_server:
                json_data['data'][str(guild)] = []
                db = DiscordGuild(guild)
                league_list = db.get_reported_leagues()

                for leagueId in league_list:
                    league_data = db.get_league_data(leagueId)
                    if league_data is not None:
                        platform_id = league_data[3]
                        league_id = league_data[1]

                        league_data = league_db.get_league_data(league_id, platform_id)
                        if league_data.get('date_last_match') is None:
                            json_data['data'][str(guild)].append(
                                {
                                    'name': league_data.get('name'),
                                    'id': league_data.get('id'),
                                    'platform_id': league_data.get('platform_id'),
                                    'reason': 'No field date_last_match'
                                }
                            )
                            db.remove_competition_in_league(league_id, platform_id)
                            nb_cleared += 1
                        elif league_data.get('date_last_match') < '2022-01-01 00:00:00':
                            json_data['data'][str(guild)].append(
                                {
                                    'name': league_data.get('name'),
                                    'id': league_data.get('id'),
                                    'platform_id': league_data.get('platform_id'),
                                    'date_last_match': league_data.get('date_last_match'),
                                    'reason': 'League too old'
                                }
                            )
                            db.remove_competition_in_league(league_id, platform_id)
                            nb_cleared += 1
        except Exception as e:
            print(e)

    json_data['msg'] = f"{nb_cleared} Leagues cleared:"
    return json_data
