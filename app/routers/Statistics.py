import math
from typing import Optional

from fastapi import APIRouter, Header, HTTPException, status
from routers import verify_token
from routers.Coaches import get_coach
from spike_database.BB2Resources import BB2Resources
from spike_database.CoachesStatistics import CoachesStatistics, StatisticType
from spike_database.MatchHistory import MatchHistory
from spike_utilities.Statistics import Statistics as StatUtils, STATISTICS_VERSION

statistics = APIRouter()
router_name = 'Statistics'


@statistics.get('/platforms/{platform_id}/coaches/{coach_id}/win_rate', tags=[router_name])
async def get_win_rate(platform_id: int, coach_id: int,
                       race_id: Optional[int] = None, league_name: Optional[str] = None,
                       competition_name: Optional[str] = None, tv_range: Optional[str] = 'all_tv',
                       top_race_limit: Optional[int] = 0, top_league_limit: Optional[int] = 0,
                       api_key: str = Header(None, alias='API_KEY')):
    verify_token(api_key, 'statistics.read')

    # Check coach exist
    await get_coach(platform_id, coach_id, api_key)

    coach_stat_db = CoachesStatistics()
    rsrc_db = BB2Resources()

    stat_type = StatisticType.OVERALL
    entry_name = None
    if league_name is not None:
        stat_type = StatisticType.LEAGUE
        entry_name = league_name
    elif competition_name is not None:
        stat_type = StatisticType.COMPETITION
        entry_name = competition_name

    if race_id is not None:
        race_label = rsrc_db.get_race_label(race_id)
    else:
        race_label = None

    coach_statistics = coach_stat_db.get_coach_statistic(coach_id, platform_id, stat_type, race_label, tv_range, entry_name, STATISTICS_VERSION)
    if coach_statistics is None and not coach_stat_db.coach_stat_exist(coach_id, platform_id, STATISTICS_VERSION):
        StatUtils.compute_and_save_coaches_win_rate(coach_id, platform_id)
        coach_statistics = coach_stat_db.get_coach_statistic(coach_id, platform_id, stat_type, race_label, tv_range, entry_name, STATISTICS_VERSION)

    if coach_statistics is None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail='Not found')

    if race_label is not None:
        data = {
            'win_rate': coach_statistics.get('overall', {}).get('races_for', {}).get(race_label, {}).get('win_rate', 0),
            'nb_win': coach_statistics.get('overall', {}).get('races_for', {}).get(race_label, {}).get('win', 0),
            'nb_draw': coach_statistics.get('overall', {}).get('races_for', {}).get(race_label, {}).get('draw', 0),
            'nb_loss': coach_statistics.get('overall', {}).get('races_for', {}).get(race_label, {}).get('loss', 0),
            'nb_match': coach_statistics.get('overall', {}).get('races_for', {}).get(race_label, {}).get('games_played',
                                                                                                         0)}
    else:
        data = {'win_rate': coach_statistics.get('overall', {}).get('win_rate', 0),
                'nb_win': coach_statistics.get('overall', {}).get('win', 0),
                'nb_draw': coach_statistics.get('overall', {}).get('draw', 0),
                'nb_loss': coach_statistics.get('overall', {}).get('loss', 0),
                'nb_match': coach_statistics.get('overall', {}).get('games_played', 0)}

    if top_race_limit > 0:
        race_list = list(coach_statistics.get('overall', {}).get('races_for', {}).values())
        race_list = sorted(race_list, key=lambda i: i['games_played'], reverse=True)
        data['top_races'] = []
        for race in race_list[:top_race_limit]:
            race_stat = {
                'name': race.get('name'),
                'win_rate': race.get('win_rate'),
                'nb_win': race.get('win'),
                'nb_draw': race.get('draw'),
                'nb_loss': race.get('loss'),
                'nb_match': race.get('games_played')
            }
            data['top_races'].append(race_stat)

    if top_league_limit > 0:
        top_leagues = coach_stat_db.get_most_played(coach_id, platform_id, StatisticType.LEAGUE, tv_range=tv_range, limit=top_league_limit,
                                                    version=STATISTICS_VERSION)
        data['top_leagues'] = []
        for league in top_leagues:
            league_stat = {
                'name': league.get('overall', {}).get('name'),
                'win_rate': league.get('overall', {}).get('win_rate', 0),
                'nb_win': league.get('overall', {}).get('win', 0),
                'nb_draw': league.get('overall', {}).get('draw', 0),
                'nb_loss': league.get('overall', {}).get('loss', 0),
                'nb_match': league.get('overall', {}).get('games_played', 0)
            }
            data['top_leagues'].append(league_stat)

    return data


@statistics.get('/platforms/{platform_id}/coaches/{coach1_id}/vs/{coach2_id}', tags=[router_name])
async def get_versus(platform_id: int, coach1_id: int, coach2_id: int,
                     league_name: Optional[str] = None, competition_name: Optional[str] = None,
                     display_match_list: bool = False,
                     api_key: str = Header(None, alias='API_KEY')):
    verify_token(api_key, 'statistics.read')
    json_data = {
        'data': None
    }

    match_history_db = MatchHistory()
    res = match_history_db.get_vs_history(coach1_id, coach2_id, platform_id, league_name, competition_name)

    if len(res) == 0:
        json_data['msg'] = 'No game found between these two coaches.'
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=json_data)

    league_stats = {}
    competition_stats = {}
    win_coach1 = 0
    win_coach2 = 0
    draw = 0
    match_list = []

    for match in res:
        competition_key = f"{match['league_name']}-{match['competition_name']}"
        if competition_stats.get(competition_key) is None:
            competition_stats[competition_key] = {
                'competition_name': match['competition_name'],
                'league_name': match['league_name'],
                'win_coach1': 0,
                'draw': 0,
                'win_coach2': 0,
                'matches': []
            }

        if league_stats.get(match['league_name']) is None:
            league_stats[match['league_name']] = {
                'league_name': match['league_name'],
                'win_coach1': 0,
                'draw': 0,
                'win_coach2': 0,
                'matches': []
            }

        if display_match_list:
            match_list.append(match)
            competition_stats[competition_key]['matches'].append(match)
            league_stats[match['league_name']]['matches'].append(match)

        if match['team_away']['score'] == match['team_home']['score']:
            draw += 1
            competition_stats[competition_key]['draw'] += 1
            league_stats[match['league_name']]['draw'] += 1
        elif (match['team_away']['coach_id'] == coach1_id and match['team_away']['score'] > match['team_home']['score']) or (
                match['team_home']['coach_id'] == coach1_id and match['team_away']['score'] < match['team_home']['score']):
            win_coach1 += 1
            competition_stats[competition_key]['win_coach1'] += 1
            league_stats[match['league_name']]['win_coach1'] += 1
        else:
            win_coach2 += 1
            competition_stats[competition_key]['win_coach2'] += 1
            league_stats[match['league_name']]['win_coach2'] += 1
    stats = {
        'leagues': list(league_stats.values()),
        'competitions': list(competition_stats.values()),
        'win_coach1': win_coach1,
        'draw': draw,
        'win_coach2': win_coach2,
        'matches': match_list
    }
    json_data['data'] = stats
    return json_data


@statistics.get('/compute_rank', tags=[router_name])
async def compute_rank(win: int = 0, draw: int = 0, loss: int = 0, api_key: str = Header(None, alias='API_KEY')):
    verify_token(api_key, 'statistics.read')
    json_data = {'data': None}

    nb_games = win + draw + loss

    if nb_games == 0:
        json_data['msg'] = 'Invalid request win, draw and loss are equal to 0.'
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=json_data)

    cross_point = 0.2
    limit = 42
    a = 0.05
    target = 28
    x = math.log(a / (1 - cross_point), 10) / math.log(1 - (target / limit), 10)

    win_pct = 100 * (win + draw / 2) / nb_games
    rank_points = (cross_point + (1 - cross_point) * (1 - ((1 - (0.5 * ((nb_games + limit) - math.sqrt((nb_games - limit) ** 2)) / limit)) ** x)))
    rank_points *= win_pct
    rank_points += nb_games * 0.02
    rank = round(rank_points, 2)

    json_data['data'] = rank
    json_data['msg'] = f"Your rank is: {rank}"
    return json_data
