from db.SqliteCompetition import SqliteCompetition
from db.SqliteLeague import SqliteLeague
from fastapi import APIRouter, Body, Header, HTTPException, status as http_status
from models.GuildsModel import CompRegistration
from routers import verify_token
from routers.Competitions import get_competition
from routers.Leagues import get_league
from spike_database.Competitions import Competitions
from spike_database.Leagues import Leagues
from spike_database.ResourcesRequest import ResourcesRequest
from spike_model.Competition import Competition
from spike_model.League import League
from spike_requester.CyanideApi import CyanideApi

guilds = APIRouter()
router_name = 'Guilds'


@guilds.get('/guilds/{guild_id}/leagues', tags=[router_name], status_code=http_status.HTTP_200_OK)
async def get_registered_leagues(guild_id: str, api_key: str = Header(None, alias='API_KEY')):
    verify_token(api_key, 'guilds.read')
    json_data = {
        'data': None
    }
    sqlite_db = SqliteLeague(guild_id)
    rsrc_db = ResourcesRequest()
    ret = sqlite_db.get_leagues()

    if ret is None:
        json_data['msg'] = f"Internal configuration error, guild id: {guild_id}, you can report this bug in spike discord (#bug-report)."
        raise HTTPException(status_code=http_status.HTTP_500_INTERNAL_SERVER_ERROR, detail=json_data)

    if len(ret) == 0:
        json_data['msg'] = 'No league registered.'
        raise HTTPException(status_code=http_status.HTTP_404_NOT_FOUND, detail=json_data)

    leagues = []
    for league in ret:
        leagues.append(
            {
                '_id': league[0],
                'league_id': league[1],
                'name': league[2],
                'platform_id': league[3],
                'platform': rsrc_db.get_platform_label(league[3]),
                'platform_emote': rsrc_db.get_platform(league[3])[2],
                'logo': league[4],
                'league_emote': rsrc_db.get_team_emoji(league[4])
            }
        )
    json_data['data'] = leagues
    json_data['msg'] = f"{len(leagues)} leagues registered in this guild"
    return json_data


@guilds.post('/guilds/{guild_id}/platform/{platform_id}/leagues/{league_id}/add', tags=[router_name],
             status_code=http_status.HTTP_200_OK)
async def add_league(guild_id: str, platform_id: int, league_id: int, api_key: str = Header(None, alias='API_KEY')):
    verify_token(api_key, 'guilds.write')

    json_data = {
        'data': None
    }
    guild_db = SqliteLeague(guild_id)
    rsrc_db = ResourcesRequest()
    platform = rsrc_db.get_platform_label(platform_id)
    ret = guild_db.is_league_registered(league_id, platform_id)

    if ret is True:
        json_data['msg'] = 'League already registered, unable to add.'
        raise HTTPException(status_code=http_status.HTTP_400_BAD_REQUEST, detail=json_data)

    if ret is None:
        json_data[
            'msg'] = f"Internal configuration error, guild id: {guild_id}, you can report this bug in spike discord (#bug-report)."
        raise HTTPException(status_code=http_status.HTTP_500_INTERNAL_SERVER_ERROR, detail=json_data)

    league_db = Leagues()
    api = CyanideApi()
    league_data = api.get_league(platform=platform, league_id=league_id)
    league_model = League(league_data.get('league'), platform)
    league_db.add_or_update_league(league_model.get_name(), league_model.get_id(), league_model.get_platform_id(),
                                   league_model.get_logo())
    guild_db.add_league(league_model.get_name(), league_model.get_platform_id(), league_model.get_id(),
                        league_model.get_logo())

    json_data['data'] = league_data
    json_data['msg'] = f"League {league_model.get_name()} successfully registered."
    return json_data


@guilds.delete('/guilds/{guild_id}/platform/{platform_id}/leagues/{league_id}/remove', tags=[router_name],
               status_code=http_status.HTTP_200_OK)
async def remove_league(guild_id: str, platform_id: int, league_id: int, api_key: str = Header(None, alias='API_KEY')):
    verify_token(api_key, 'guilds.write')

    json_data = {
        'data': None
    }
    guild_db = SqliteLeague(guild_id)
    comp_db = SqliteCompetition(guild_id)
    rsrc_db = ResourcesRequest()
    league_db = Leagues()
    league_data = league_db.get_league_data(league_id, platform_id)
    if league_data is None:
        json_data['msg'] = 'League not found.'
        raise HTTPException(status_code=http_status.HTTP_404_NOT_FOUND, detail=json_data)

    league_model = League(league_data, rsrc_db.get_platform_label(platform_id))
    ret = guild_db.is_league_registered(league_id, platform_id)

    if ret is False:
        json_data['msg'] = 'League not registered, unable to remove.'
        raise HTTPException(status_code=http_status.HTTP_404_NOT_FOUND, detail=json_data)

    if ret is None:
        json_data[
            'msg'] = f"Internal configuration error, guild id: {guild_id}, you can report this bug in spike discord (#bug-report)."
        raise HTTPException(status_code=http_status.HTTP_500_INTERNAL_SERVER_ERROR, detail=json_data)

    ret = guild_db.remove_league(league_model.get_id(), league_model.get_platform_id())

    if ret is None or ret.rowcount == 0:
        json_data['msg'] = 'League not registered, unable to remove.'
        raise HTTPException(status_code=http_status.HTTP_404_NOT_FOUND, detail=json_data)

    comp_db.remove_competition_in_league(league_model.get_id(), league_model.get_platform_id())

    json_data['data'] = league_data
    json_data['msg'] = f"League {league_model.get_name()} successfully removed."
    return json_data


@guilds.get('/guilds/{guild_id}/competitions', tags=[router_name], status_code=http_status.HTTP_200_OK)
async def get_registered_competitions(guild_id: str, api_key: str = Header(None, alias='API_KEY')):
    verify_token(api_key, 'guilds.read')

    json_data = {
        'data': None
    }
    leagues = await get_registered_leagues(guild_id, api_key)
    leagues = leagues.get('data', [])
    sqlite_cmp_db = SqliteCompetition(guild_id)
    nb_cmp = 0

    for league in leagues:
        league['competitions'] = []
        ret = sqlite_cmp_db.get_competitions(league.get('league_id'), league.get('platform_id'))

        if ret is None:
            json_data[
                'msg'] = f"Internal configuration error, guild id: {guild_id}, you can report this bug in spike discord (#bug-report)."
            raise HTTPException(status_code=http_status.HTTP_500_INTERNAL_SERVER_ERROR, detail=json_data)

        if len(ret) > 0:
            for competition in ret:
                league['competitions'].append(
                    {
                        '_id': competition[0],
                        'competition_id': competition[1],
                        'name': competition[2],
                        'logo_link': competition[4],
                        'logo_emote': competition[5],
                        'channel_id': str(competition[3]),
                        'platform_id': league.get('platform_id')
                    }
                )
            nb_cmp += len(league['competitions'])

    json_data['data'] = leagues
    json_data['nb_competitions'] = nb_cmp
    json_data['msg'] = f"{nb_cmp} competitions registered in this server."
    return json_data


@guilds.post('/guilds/{guild_id}/platform/{platform_id}/leagues/{league_id}/competitions/{competition_id}/add',
             tags=[router_name], status_code=http_status.HTTP_200_OK)
async def add_competition(guild_id: str, platform_id: int, league_id: int, competition_id: int,
                          cmp_reg: CompRegistration = Body(), api_key: str = Header(None, alias='API_KEY')):
    verify_token(api_key, 'guilds.write')
    json_data = {
        'data': None
    }

    guild_db = SqliteCompetition(guild_id)
    rsrc_db = ResourcesRequest()
    platform = rsrc_db.get_platform_label(platform_id)
    emoji_id = rsrc_db.get_team_emoji(cmp_reg.logo_label)
    emoji_url = rsrc_db.get_team_emoji_url(cmp_reg.logo_label)
    ret = guild_db.is_competition_registered(competition_id, platform_id)

    if ret is True:
        json_data['msg'] = 'Competition already registered, unable to add.'
        raise HTTPException(status_code=http_status.HTTP_400_BAD_REQUEST, detail=json_data)

    if ret is None:
        json_data['msg'] = f"Internal configuration error, guild id: {guild_id}, you can report this bug in spike discord (#bug-report)."
        raise HTTPException(status_code=http_status.HTTP_500_INTERNAL_SERVER_ERROR, detail=json_data)

    competition_data = {}
    try:
        competition_data = await get_competition(platform_id, competition_id, api_key)
        competition_data = competition_data.get('data', {})
        competition_data.pop('standing', None)
        competition_data.pop('standing_source', None)
        competition_data.pop('standing_update', None)
        competition_data.pop('point_system', None)
    except HTTPException:
        competition_db = Competitions()
        league_data = await get_league(platform_id, league_id, api_key)
        league_model = League(league_data.get('league', {}), platform)
        api = CyanideApi()
        competitions = api.get_competitions(league_model.get_name(), 500, league_model.get_platform())
        for competition in competitions.get('competitions', []):
            cmp_model = Competition(competition)
            competition_db.add_or_update_raw_competition(cmp_model, league_model.get_platform_id())
            if cmp_model.get_id() == competition_id:
                competition_data = competition

    cmp_model = Competition(competition_data)
    guild_db.add_competition(cmp_model.get_id(), cmp_model.get_name(), cmp_reg.channel_id, emoji_url, emoji_id,
                             platform_id, league_id)

    json_data['data'] = competition_data
    json_data['msg'] = f"Competition {cmp_model.get_name()} successfully registered."
    return json_data


@guilds.delete('/guilds/{guild_id}/platform/{platform_id}/competitions/{competition_id}/remove', tags=[router_name],
               status_code=http_status.HTTP_200_OK)
async def remove_competition(guild_id: str, platform_id: int, competition_id: int,
                             api_key: str = Header(None, alias='API_KEY')):
    verify_token(api_key, 'guilds.write')
    json_data = {
        'data': None
    }
    guild_db = SqliteCompetition(guild_id)
    competition_db = Competitions()
    competition_data = competition_db.get_competition_data(competition_id, platform_id)
    if competition_data is None:
        json_data['msg'] = 'Competition not found.'
        raise HTTPException(status_code=http_status.HTTP_404_NOT_FOUND, detail=json_data)

    competition_data.pop('standing', None)
    competition_data.pop('standing_source', None)
    competition_data.pop('standing_update', None)
    competition_data.pop('point_system', None)

    competition_model = Competition(competition_data)
    ret = guild_db.is_competition_registered(competition_id, platform_id)

    if ret is False:
        json_data['msg'] = 'Competition not registered, unable to remove.'
        raise HTTPException(status_code=http_status.HTTP_404_NOT_FOUND, detail=json_data)

    if ret is None:
        json_data[
            'msg'] = f"Internal configuration error, guild id: {guild_id}, you can report this bug in spike discord (#bug-report)."
        raise HTTPException(status_code=http_status.HTTP_500_INTERNAL_SERVER_ERROR, detail=json_data)

    ret = guild_db.remove_competition(competition_model.get_id(), platform_id)

    if ret is None or ret.rowcount == 0:
        json_data['msg'] = 'League not registered, unable to remove.'
        raise HTTPException(status_code=http_status.HTTP_404_NOT_FOUND, detail=json_data)

    json_data['data'] = competition_data
    json_data['msg'] = f"Competition {competition_model.get_name()} successfully removed."
    return json_data
