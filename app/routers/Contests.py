from fastapi import APIRouter, Header, HTTPException, status as http_status

from routers import verify_token
from spike_database.Contests import Contests

contests = APIRouter()
router_name = 'Contests'


@contests.get('/contests/{_id}', tags=[router_name])
async def get_contest(_id: str, api_key: str = Header(None, alias='API_KEY')):
    verify_token(api_key, 'contests.read')
    json_data = {'data': None}

    contest_db = Contests()
    contest_data = contest_db.get_contest_by_id(_id)
    if contest_data is None:
        json_data['msg'] = 'Contest not found.'
        raise HTTPException(status_code=http_status.HTTP_404_NOT_FOUND, detail=json_data)
    contest_data['_id'] = str(contest_data['_id'])

    json_data['msg'] = 'Contest found.'
    json_data['data'] = contest_data
    return json_data
