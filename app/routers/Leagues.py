from typing import Optional

from fastapi import APIRouter, Header, HTTPException, BackgroundTasks, status as http_status
from fastapi.encoders import jsonable_encoder

from models.LeaguesModel import LeaguesModel
from routers import verify_token
from spike_database.Competitions import Competitions
from spike_database.Leagues import Leagues
from spike_database.ResourcesRequest import ResourcesRequest
from spike_model.League import League
from spike_utilities.LeagueUtilities import LeagueUtilities

leagues = APIRouter()
router_name = 'Leagues'


@leagues.get('/platforms/{platform_id}/leagues/{league_id}/competitions', tags=[router_name])
async def get_competitions(platform_id: int, league_id: int, status: Optional[int] = 1, api_key: str = Header(None, alias='API_KEY')):
    verify_token(api_key, 'competitions.read')
    json_data = {
        'data': None
    }

    competition_db = Competitions()
    competitions = competition_db.get_competitions_in_league(league_id, platform_id, status)
    if len(competitions) > 0:
        json_data['data'] = []
        for competition in competitions:
            competition.pop('standing', None)
            competition.pop('point_system', None)
            competition.pop('standing_source', None)
            competition.pop('standing_update', None)
            competition.pop('top_races', None)
            competition.pop('active_ban', None)
            competition.pop('inactive_ban', None)
            competition.pop('contest_fully_collected', None)
            json_data['data'].append(competition)

        json_data['msg'] = f"{len(competitions)} competitions found in this league"
        return json_data
    json_data['msg'] = 'No competition found in this league.'
    raise HTTPException(status_code=http_status.HTTP_404_NOT_FOUND, detail=json_data)


@leagues.get('/platforms/{platform_id}/leagues/{league_id}', tags=[router_name])
async def get_league(platform_id: int, league_id: int, api_key: str = Header(None, alias='API_KEY')):
    verify_token(api_key, 'leagues.read')
    json_data = {
        'data': None
    }

    rsrc_db = ResourcesRequest()
    league_db = Leagues()
    league_data = league_db.get_league_data(league_id, platform_id)
    if league_data is not None:
        if league_data.get('logo') is not None:
            league_data['league_emote'] = rsrc_db.get_team_emoji(league_data.get('logo'))

        league_data.pop('ccl_playoffs', None)
        league_data.pop('ccl_season', None)
        league_data.pop('admins', None)
        league_data.pop('competitions', None)

        json_data['msg'] = 'League found successfully.'
        json_data['data'] = league_data
        return json_data
    json_data['msg'] = 'League not found.'
    raise HTTPException(status_code=http_status.HTTP_404_NOT_FOUND, detail=json_data)


@leagues.post('/platforms/{platform_id}/leagues/add', tags=[router_name])
async def add_league(platform_id: int, league_data: LeaguesModel, api_key: str = Header(None, alias='API_KEY')):
    verify_token(api_key, 'leagues.write')
    json_data = {
        'data': None
    }

    league_data = jsonable_encoder(league_data)
    league_data['platform_id'] = platform_id
    cmp_league = League(league_data)
    league_db = Leagues()
    league_db.add_or_update_raw_league(cmp_league, platform_id)
    json_data['msg'] = f"League {cmp_league.get_name()} added."
    json_data['data'] = league_data
    return json_data


@leagues.patch('/platforms/{platform_id}/leagues/{league_id}/update', tags=[router_name])
async def update_league(platform_id: int, league_id: int, background_tasks: BackgroundTasks,
                        api_key: str = Header(None, alias='API_KEY')):
    verify_token(api_key, 'leagues.read')
    json_data = {
        'data': None
    }
    background_tasks.add_task(LeagueUtilities.update_league, league_id, platform_id, False)
    json_data['msg'] = 'League update in progress.'
    return json_data
