from ApiKey import ApiKey
from fastapi import HTTPException, status as http_status


def verify_token(token: str, scope: str):
    json_data = {"data": None}
    if token is None:
        json_data["msg"] = "No API key provided."
        raise HTTPException(status_code=http_status.HTTP_401_UNAUTHORIZED, detail=json_data)
    keys = ApiKey.get_api_keys()
    match = next((item for item in keys if item.get("key") == token), None)
    if match is None:
        json_data["msg"] = "Invalid API key provided."
        raise HTTPException(status_code=http_status.HTTP_401_UNAUTHORIZED, detail=json_data)

    if scope not in match.get("scopes", []):
        json_data["msg"] = f"Endpoint not allowed, {scope} required."
        raise HTTPException(status_code=http_status.HTTP_403_FORBIDDEN, detail=json_data)
