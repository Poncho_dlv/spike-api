from typing import Optional

from fastapi import APIRouter, HTTPException, Header, status
from routers import Competitions as CompetitionAPI
from routers import verify_token
from spike_database.BB2Resources import BB2Resources
from spike_database.CoachesStatistics import CoachesStatistics, StatisticType
from spike_database.Competitions import Competitions
from spike_database.Leagues import Leagues
from spike_utilities.Statistics import Statistics, STATISTICS_VERSION

ccl = APIRouter()
router_name = 'CCL'


def get_ccl_league_id(platform_id: int):
    if platform_id in [1, 2]:
        return 1
    if platform_id == 3:
        return 3
    return None


def get_ccl_season_id(platform_id: int, season_index: int, competition_type: str):
    json_data = {'data': None}
    league_db = Leagues()
    league_id = get_ccl_league_id(platform_id)
    league_data = league_db.get_league_data(league_id, platform_id)
    if league_data is None:
        json_data['msg'] = 'League not found.'
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=json_data)

    nb_season = len(league_data.get(competition_type, []))
    if season_index > nb_season:
        json_data['msg'] = f"Invalid season index value, maximum value is {nb_season}."
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=json_data)

    index = nb_season - season_index
    competition_id = league_data.get(competition_type)[index]

    if competition_id is None:
        json_data['msg'] = f"Invalid competition id for season {season_index}."
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=json_data)
    return competition_id


def get_ccl_season_data(platform_id: int, season_index: int, competition_type: str):
    json_data = {'data': None}
    competition_id = get_ccl_season_id(platform_id, season_index, competition_type)
    competition_db = Competitions()

    competition_data = competition_db.get_competition_data(competition_id, platform_id)
    if competition_data is None:
        json_data['msg'] = f"Competition data not found for season {season_index}."
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=json_data)
    return competition_data


@ccl.get('/platforms/{platform_id}/ccl/current_season/ban_list', tags=[router_name])
async def get_ban_list(platform_id: int, api_key: str = Header(None, alias='API_KEY')):
    verify_token(api_key, 'ccl.read')
    json_data = {'data': None}

    competition_db = Competitions()
    current_season = await get_current_season(platform_id, api_key)
    competition_id = current_season.get('data', {}).get('ladder', {}).get('id')
    competition_data = competition_db.get_competition_data(competition_id, platform_id)

    if competition_data is not None:
        json_data['msg'] = f"{competition_data.get('name')} ban list."
        json_data['data'] = {
            'active_ban': competition_data.get('active_ban', {}),
            'competition_id': competition_id,
            'competition_name': competition_data.get('name')}
        return json_data

    json_data['msg'] = 'Competition not found.'
    raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=json_data)


@ccl.get('/platforms/{platform_id}/ccl/current_season', tags=[router_name])
async def get_current_season(platform_id: int, api_key: str = Header(None, alias='API_KEY')):
    verify_token(api_key, 'ccl.read')
    json_data = {
        'data': None
    }

    league_db = Leagues()
    competition_db = Competitions()

    league_id = get_ccl_league_id(platform_id)
    league_data = league_db.get_league_data(league_id, platform_id)

    if league_data is None:
        json_data['msg'] = 'Current season information not found.'
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=json_data)

    ladder_id = league_data.get('ccl_season', [None])[0]
    playoffs_id = league_data.get('ccl_playoffs', [None])[0]

    ladder = competition_db.get_competition_data(ladder_id, platform_id)
    if ladder is None or ladder.get('status') != 1:
        ladder_id = league_data.get('ccl_season', [None, None])[1]
        ladder = competition_db.get_competition_data(ladder_id, platform_id)

    playoff = competition_db.get_competition_data(playoffs_id, platform_id)
    if playoff is None or playoff.get('status') != 1:
        playoffs_id = league_data.get('ccl_playoffs', [None, None])[1]
        playoff = competition_db.get_competition_data(playoffs_id, platform_id)

    ret = {
        'ladder': {
            'name': ladder.get('name'),
            'id': ladder.get('id'),
        },
        'playoffs': {
            'name': playoff.get('name'),
            'id': playoff.get('id'),
        }
    }

    json_data['msg'] = ''
    json_data['data'] = ret
    return json_data


@ccl.get('/platforms/{platform_id}/ccl/seasons', tags=[router_name])
async def get_seasons(platform_id: int, api_key: str = Header(None, alias='API_KEY')):
    verify_token(api_key, 'ccl.read')
    json_data = {
        'data': None
    }

    league_db = Leagues()
    competition_db = Competitions()

    league_id = get_ccl_league_id(platform_id)
    league_data = league_db.get_league_data(league_id, platform_id)

    if league_data is None:
        json_data['msg'] = 'Unable to find Cabalvision official league on this platform.'
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=json_data)

    ladder_ids = league_data.get('ccl_season')
    playoffs_ids = league_data.get('ccl_playoffs')

    ladder_list = competition_db.get_competitions_data(league_id, ladder_ids, platform_id)
    playoffs_list = competition_db.get_competitions_data(league_id, playoffs_ids, platform_id)
    ret = {
        'ladders': [],
        'playoffs': []
    }

    for index, ladder in enumerate(ladder_list, 1):
        ladder_data = {
            'name': ladder.get('name'),
            'id': ladder.get('id'),
            'season_number': index
        }
        ret['ladders'].append(ladder_data)

    modifier = 0
    for index, playoff in enumerate(playoffs_list, 1):
        if index == 8:
            playoff_data = {
                'name': 'Champion Cup VIII',
                'id': None,
                'season_number': index,
                'info': 'Canceled due to legendary edition release'
            }
            ret['playoffs'].append(playoff_data)
            modifier = 1

        playoff_data = {
            'name': playoff.get('name'),
            'id': playoff.get('id'),
            'season_number': index + modifier
        }
        ret['playoffs'].append(playoff_data)
    json_data['data'] = ret
    return json_data


@ccl.get('/platforms/{platform_id}/ccl/seasons/{season_index}/ranking', tags=[router_name])
async def get_ranking(platform_id: int, season_index: int, limit: Optional[int] = 50, api_key: str = Header(None, alias='API_KEY')):
    verify_token(api_key, 'ccl.read')
    json_data = {'data': None}
    competition_data = get_ccl_season_data(platform_id, season_index, 'ccl_season')
    json_data['data'] = {
        'name': competition_data.get('name'),
        'id': competition_data.get('id'),
        'rank': competition_data.get('standing', [])[:limit]
    }
    return json_data


@ccl.get('/platforms/{platform_id}/ccl/seasons/{season_index}/playoffs_bracket', tags=[router_name])
async def get_playoffs_bracket(platform_id: int, season_index: int, round_index: Optional[int] = None, api_key: str = Header(None, alias='API_KEY')):
    verify_token(api_key, 'ccl.read')
    competition_id = get_ccl_season_id(platform_id, season_index, 'ccl_playoffs')
    return await CompetitionAPI.get_contests(platform_id, competition_id, round_number=round_index, api_key=api_key)


@ccl.get('/platforms/{platform_id}/ccl/current_season/predictions', tags=[router_name])
async def get_predictions(platform_id: Optional[int] = 1, api_key: str = Header(None, alias='API_KEY')):
    verify_token(api_key, 'ccl.read')
    json_data = {'data': None}

    competition_db = Competitions()
    current_season = await get_current_season(platform_id, api_key)
    competition_id = current_season.get('data', {}).get('ladder', {}).get('id')
    competition_data = competition_db.get_standing(competition_id, platform_id)

    if competition_data is None:
        json_data['msg'] = 'Current season data not found.'
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=json_data)

    if platform_id == 1:
        number_of_race = 24
        available_spot = 64
        max_per_race = 4
    elif platform_id == 2:
        number_of_race = 24
        available_spot = 32
        max_per_race = 2
    else:
        number_of_race = 24
        available_spot = 32
        max_per_race = 5

    team_list = []
    coaches = []
    race_statistics = {}

    json_data['rules'] = {
        'number_of_race': number_of_race,
        'available_spot': available_spot,
        'max_per_race': max_per_race,
        'url': None
    }

    if platform_id == 1:
        json_data['rules']['url'] = 'https://docs.google.com/document/d/15k0GFBwMDPO9eGUCIrd72zqCaERA3IFH6phWdt6CkJc/edit?usp=sharing'
        for team in competition_data.get('standing', []):
            if team.get('coach_id') not in coaches and race_statistics.get(team.get('race'), {}).get('number', 0) < 2:
                if race_statistics.get(team.get('race')) is None:
                    team_list.append(team)
                    coaches.append(team.get('coach_id'))
                    race_statistics[team.get('race')] = {
                        'name': team.get('race'),
                        'number': 1,
                        'min_rank': team.get('sort'),
                        'max_rank': team.get('sort')
                    }
                elif race_statistics.get(team.get('race'), {}).get('number', 0) == 2 and (team.get('sort') >= 70 or (
                        team.get('race') in ['Ogre', 'Underworld', 'Halfling'] and team.get('sort') >= 65)):
                    team_list.append(team)
                    coaches.append(team.get('coach_id'))
                    race_statistics[team.get('race')]['number'] += 1
                    race_statistics[team.get('race')]['min_rank'] = min(race_statistics.get(team['race'], {}).get('min_rank'), team.get('sort'))
                    race_statistics[team.get('race')]['max_rank'] = max(race_statistics.get(team['race'], {}).get('max_rank'), team.get('sort'))

    for team in competition_data.get('standing', []):
        if team.get('coach_id') not in coaches and race_statistics.get(team.get('race'), {}).get('number', 0) < max_per_race:
            if race_statistics.get(team.get('race')) is None:
                team_list.append(team)
                coaches.append(team.get('coach_id'))
                race_statistics[team.get('race')] = {
                    'name': team.get('race'),
                    'number': 1,
                    'min_rank': team.get('sort'),
                    'max_rank': team.get('sort')
                }
            elif available_spot - len(team_list) > number_of_race - len(race_statistics.keys()):
                team_list.append(team)
                coaches.append(team.get('coach_id'))
                race_statistics[team.get('race')]['number'] += 1
                race_statistics[team.get('race')]['min_rank'] = min(race_statistics.get(team['race'], {}).get('min_rank'), team.get('sort'))
                race_statistics[team.get('race')]['max_rank'] = max(race_statistics.get(team['race'], {}).get('max_rank'), team.get('sort'))

        if len(team_list) == available_spot:
            break

    race_breakdown = []
    for value in race_statistics.values():
        race_breakdown.append(value)

    race_breakdown = sorted(race_breakdown, key=lambda i: i['number'], reverse=True)
    team_list = sorted(team_list, key=lambda i: i['sort'], reverse=True)

    json_data['msg'] = "Spike Predictions for current CCL season, this is not the official list."
    json_data['data'] = {'race_breakdown': race_breakdown, 'team_list': team_list}
    return json_data


@ccl.get('/platforms/{platform_id}/ccl/seasons/{season_index}/top_races', tags=[router_name])
async def get_top_races(platform_id: int, season_index: int, limit: Optional[int] = 10,
                        race: Optional[str] = None, api_key: str = Header(None, alias='API_KEY')):
    verify_token(api_key, 'ccl.read')
    json_data = {'data': None}
    competition_data = get_ccl_season_data(platform_id, season_index, 'ccl_season')
    top_races = {}
    for key, value in competition_data.get('top_races', {}).items():
        if race is None or race.lower() == key.lower():
            teams = value[:limit]
            for team in teams:
                team.pop('casualties_inflicted', None)
                team.pop('casualties_sustained', None)
                team.pop('casualties_difference', None)
                team.pop('kill', None)
                team.pop('coleman_waldorf', None)
                team.pop('coleman_waldorf_2', None)
                team.pop('head_to_head', None)
                team.pop('opponents', None)
                team.pop('matches', None)
                team.pop('external_tie_breaker', None)
                team.pop('coleman_waldorf_3', None)
                team.pop('td_for', None)
                team.pop('td_against', None)
                team.pop('tdd', None)
            top_races[key] = teams
    json_data['data'] = {
        'name': competition_data.get('name'),
        'id': competition_data.get('id'),
        'races_rank': top_races
    }
    return json_data


@ccl.get('/platforms/{platform_id}/ccl/coaches/{coach_id}/win_rate', tags=[router_name])
async def get_win_rate(platform_id: int, coach_id: int, race_id: Optional[int] = None, season_number: Optional[int] = None,
                       tv_range: Optional[str] = 'all_tv', top_race_limit: Optional[int] = 0, api_key: str = Header(None, alias='API_KEY')):
    verify_token(api_key, 'ccl.read')
    json_data = {
        'data': None
    }

    coach_stat_db = CoachesStatistics()
    rsrc_db = BB2Resources()

    entry_name = None
    if season_number is not None:
        stat_type = StatisticType.COMPETITION
        seasons = await get_seasons(platform_id, api_key)
        for season in seasons.get('ladders', []):
            if season.get('season_number') == season_number:
                entry_name = season.get('name')
    else:
        stat_type = StatisticType.RANKED_LADDER

    if race_id is not None:
        race_label = rsrc_db.get_race_label(race_id)
    else:
        race_label = None

    coach_statistics = coach_stat_db.get_coach_statistic(coach_id, platform_id, stat_type, race_label, tv_range,
                                                         entry_name, STATISTICS_VERSION)
    if coach_statistics is None and not coach_stat_db.coach_stat_exist(coach_id, platform_id, STATISTICS_VERSION):
        Statistics.compute_and_save_coaches_win_rate(coach_id, platform_id)
        coach_statistics = coach_stat_db.get_coach_statistic(coach_id, platform_id, stat_type, race_label, tv_range,
                                                             entry_name, STATISTICS_VERSION)

    if coach_statistics is None:
        json_data['msg'] = 'No statistics found for this coach.'
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=json_data)

    if race_label is not None:
        data = {
            'win_rate': coach_statistics.get('overall', {}).get('races_for', {}).get(race_label, {}).get('win_rate', 0),
            'nb_win': coach_statistics.get('overall', {}).get('races_for', {}).get(race_label, {}).get('win', 0),
            'nb_draw': coach_statistics.get('overall', {}).get('races_for', {}).get(race_label, {}).get('draw', 0),
            'nb_loss': coach_statistics.get('overall', {}).get('races_for', {}).get(race_label, {}).get('loss', 0),
            'nb_match': coach_statistics.get('overall', {}).get('races_for', {}).get(race_label, {}).get('games_played',
                                                                                                         0)}
    else:
        data = {'win_rate': coach_statistics.get('overall', {}).get('win_rate', 0),
                'nb_win': coach_statistics.get('overall', {}).get('win', 0),
                'nb_draw': coach_statistics.get('overall', {}).get('draw', 0),
                'nb_loss': coach_statistics.get('overall', {}).get('loss', 0),
                'nb_match': coach_statistics.get('overall', {}).get('games_played', 0)}

    if top_race_limit > 0:
        race_list = list(coach_statistics.get('overall', {}).get('races_for', {}).values())
        race_list = sorted(race_list, key=lambda i: i['games_played'], reverse=True)
        data['top_races'] = []
        for race in race_list[:top_race_limit]:
            race_stat = {
                'name': race.get('name'),
                'win_rate': race.get('win_rate'),
                'nb_win': race.get('win'),
                'nb_draw': race.get('draw'),
                'nb_loss': race.get('loss'),
                'nb_match': race.get('games_played')
            }
            data['top_races'].append(race_stat)
    json_data['data'] = data
    return json_data
