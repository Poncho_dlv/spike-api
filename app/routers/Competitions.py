from typing import Optional, List

from fastapi import APIRouter, Header, HTTPException, BackgroundTasks, status as http_status
from fastapi.encoders import jsonable_encoder
from models.CompetitionsModel import CompetitionsModel
from routers import verify_token
from spike_database.Competitions import Competitions
from spike_database.Contests import Contests
from spike_model.Competition import Competition
from spike_requester.Utilities import Utilities

competitions = APIRouter()
router_name = 'Competitions'


def add_competition_task(competition_data: dict, platform_id: int):
    if competition_data is None or platform_id is None:
        print('Unable to add competition.')
        return
    competition_data = jsonable_encoder(competition_data)
    competition_data['platform_id'] = platform_id
    cmp_model = Competition(competition_data)
    competition_db = Competitions()
    competition_db.add_or_update_raw_competition(cmp_model, platform_id)
    print(f"Competitions {competition_data.get('name')} - {competition_data.get('id')} - {platform_id} updated.")


@competitions.get('/platforms/{platform_id}/competitions/{competition_id}', tags=[router_name])
async def get_competition(platform_id: int, competition_id: int, api_key: str = Header(None, alias='API_KEY')):
    verify_token(api_key, 'competitions.read')
    json_data = {
        'data': None
    }

    competition_db = Competitions()
    competition_data = competition_db.get_competition_data(competition_id, platform_id)
    if competition_data is not None:
        competition_data.pop('_id', None)
        competition_data.pop('active_ban', None)
        competition_data.pop('inactive_ban', None)
        competition_data.pop('contest_fully_collected', None)
        competition_data.pop('logo', None)
        competition_data.pop('standing', None)

        json_data['msg'] = f"Competition {competition_data.get('name')} found."
        json_data['data'] = competition_data
        return json_data
    json_data['msg'] = 'Competition not found.'
    raise HTTPException(status_code=http_status.HTTP_404_NOT_FOUND, detail=json_data)


@competitions.get('/platforms/{platform_id}/competitions/{competition_id}/contests', tags=[router_name])
async def get_contests(platform_id: int, competition_id: int,
                       status: Optional[int] = None, round_index: Optional[int] = None, started: Optional[int] = 0,
                       finished: Optional[int] = 0,
                       api_key: str = Header(None, alias='API_KEY')):
    verify_token(api_key, 'competitions.read')
    json_data = {
        'data': None
    }

    contest_db = Contests()
    contests_list = contest_db.get_contests_in_competition(competition_id, platform_id, status, round_index)
    contests_list = sorted(contests_list, key=lambda i: (i['current_round'], i['contest_id']))
    split_contest = []
    if len(contests_list) > 0:
        for contest in contests_list:
            index = int(contest['current_round'])

            contest['_id'] = str(contest['_id'])
            contest.pop('messages', None)
            contest.pop('competition_logo', None)
            contest.pop('last_update', None)

            if contest.get('team_home', {}).get('coach_id') is None or contest.get('team_away', {}).get('coach_id') is None:
                contest.pop('winner', None)
                contest.pop('team_home', None)
                contest.pop('team_away', None)
                contest.pop('odds', None)

            if contest.get('match_uuid') is not None:
                match = Utilities.get_match(contest.get('match_uuid')[0])
                if match is not None:
                    if started == 1:
                        contest['started'] = match.get_start_datetime()
                    if finished == 1:
                        contest['finished'] = match.get_finish_datetime()

            # No round_index specified we return formatted contest
            if round_index is None:
                if len(split_contest) < index:
                    split_contest.append([])
                split_contest[index - 1].append(contest)
            else:
                split_contest.append(contest)

        json_data['msg'] = f"{len(contests_list)} contests."
        json_data['data'] = split_contest
        return json_data
    json_data['msg'] = 'No contest found.'
    raise HTTPException(status_code=http_status.HTTP_404_NOT_FOUND, detail=json_data)


@competitions.get('/platforms/{platform_id}/competitions/{competition_id}/standing', tags=[router_name])
async def get_standing(platform_id: int, competition_id: int, api_key: str = Header(None, alias='API_KEY')):
    verify_token(api_key, 'competitions.read')
    json_data = {
        'data': None
    }

    competition_db = Competitions()
    competition_data = competition_db.get_competition_data(competition_id, platform_id)
    if competition_data is not None:
        if competition_data.get('format') in ['round_robin', 'swiss', 'ladder']:
            standing_data = {
                'name': competition_data.get('name'),
                'id': competition_data.get('id'),
                'standing': competition_data.get('standing'),
                'point_system': competition_data.get('point_system'),
                'standing_source': competition_data.get('standing_source')
            }

            json_data['msg'] = f"{standing_data.get('name')} standing."
            json_data['data'] = standing_data
            return json_data
        json_data['msg'] = 'Invalid competition format.'
        raise HTTPException(status_code=400, detail=json_data)
    json_data['msg'] = 'No standing found.'
    raise HTTPException(status_code=http_status.HTTP_404_NOT_FOUND, detail=json_data)


@competitions.get('/platforms/{platform_id}/competitions/{competition_id}/bracket', tags=[router_name])
async def get_bracket(platform_id: int, competition_id: int, api_key: str = Header(None, alias='API_KEY')):
    verify_token(api_key, 'competitions.read')
    json_data = {
        'data': None
    }

    competition_db = Competitions()
    competition_data = competition_db.get_competition_data(competition_id, platform_id)
    if competition_data is not None:
        bracket_data = {
            'name': competition_data.get('name'),
            'id': competition_data.get('id'),
            'standing': competition_data.get('standing'),
            'point_system': competition_data.get('point_system'),
            'standing_source': competition_data.get('standing_source')
        }

        json_data['msg'] = 'Competition bracket found.'
        json_data['data'] = bracket_data
        return json_data
    json_data['msg'] = 'Bracket not found.'
    raise HTTPException(status_code=http_status.HTTP_404_NOT_FOUND, detail=json_data)


@competitions.post('/platforms/{platform_id}/competitions/add', tags=[router_name])
async def add_competition(background_tasks: BackgroundTasks, platform_id: int, competitions_list: List[CompetitionsModel],
                          api_key: str = Header(None, alias='API_KEY')):
    verify_token(api_key, 'competitions.write')
    json_data = {
        'data': None
    }

    for competition_data in competitions_list:
        background_tasks.add_task(add_competition_task, competition_data, platform_id)
    json_data['msg'] = 'Competitions update in progress.'
    return json_data
