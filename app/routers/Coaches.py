from fastapi import APIRouter, HTTPException, status, Header, BackgroundTasks

from routers import verify_token
from spike_database.Coaches import Coaches
from spike_utilities.Statistics import Statistics

coaches = APIRouter()
router_name = 'Coaches'


@coaches.get('/platforms/{platform_id}/coaches/{coach_id}', tags=[router_name])
async def get_coach(platform_id: int, coach_id: int, api_key: str = Header(None, alias='API_KEY')):
    verify_token(api_key, 'coaches.read')
    json_data = {'data': None}

    coach_db = Coaches()
    coach_data = coach_db.get_coach_data(platform_id=platform_id, coach_id=coach_id)

    if coach_data is None:
        json_data['msg'] = 'Coach not found.'
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=json_data)

    json_data['msg'] = f"Coach {coach_data.get('name')} found."
    json_data['data'] = coach_data
    return json_data


@coaches.get('/coaches/search', tags=[router_name])
async def search_coach(search: str, platform_id: int = None, api_key: str = Header(None, alias='API_KEY')):
    verify_token(api_key, 'coaches.read')
    json_data = {'data': None}

    coach_db = Coaches()
    coach_list = coach_db.get_coaches_like(search, platform_id, False)

    if len(coach_list) == 0:
        json_data['msg'] = '0 coach found.'
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=json_data)

    json_data['msg'] = f"{len(coach_list)} coaches found."
    json_data['data'] = coach_list
    return json_data


@coaches.patch('/platforms/{platform_id}/coaches/{coach_id}/update_stats', tags=[router_name])
async def update_stats(background_tasks: BackgroundTasks, platform_id: int, coach_id: int, api_key: str = Header(None, alias='API_KEY')):
    verify_token(api_key, 'coaches.write')
    json_data = {'data': None}
    background_tasks.add_task(Statistics.compute_and_save_coaches_win_rate(coach_id, platform_id))
    json_data['msg'] = 'Coach update in progress.'
    return json_data
