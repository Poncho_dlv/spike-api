import uvicorn
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from routers import Ccl, Leagues, Competitions, Statistics, Guilds, Coaches, Contests, Admins

app = FastAPI(title="Spike API",
              description="Spike API for Blood bowl 2, you have to ask Poncho for access.",
              version="1.0.0",
              contact={
                  "name": "Poncho",
                  "email": "poncho_dlv@pm.me",
                  "url": "https://discord.gg/8AJh4hx",
              },
              redoc_url=None,
              )

origins = [
    "http://localhost",
    "http://localhost:8080",
    "https://spike.ovh",
    "idp_api",
    "discord_bot"
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(Admins.admins)
app.include_router(Ccl.ccl)
app.include_router(Coaches.coaches)
app.include_router(Competitions.competitions)
app.include_router(Contests.contests)
app.include_router(Guilds.guilds)
app.include_router(Leagues.leagues)
app.include_router(Statistics.statistics)

if __name__ == "__main__":
    uvicorn.run(app, host="127.0.0.1", port=8000)
