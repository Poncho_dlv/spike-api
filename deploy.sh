#!/bin/bash

git clone https://Poncho_dlv:"$1"@bitbucket.org/Poncho_dlv/spike-api.git

# shellcheck disable=SC2164
cd spike-api

git submodule update --init --force --remote

docker-compose up --force-recreate --build -d

# shellcheck disable=SC2103
cd ..

rm -R spike-api/ -f