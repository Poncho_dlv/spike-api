FROM tiangolo/uvicorn-gunicorn-fastapi:python3.9


COPY requirements.txt .

# Install the dependencies
RUN pip install --no-cache-dir  -r requirements.txt  \
    && pip install --no-cache-dir  email-validator==1.3.0


COPY ./app /app

# Set the working directory
WORKDIR /app